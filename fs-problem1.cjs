// const fs = require("fs")
// const path = require("path")

// const problem1Fn = (directoryPath, numFiles) => {
//   fs.mkdir(directoryPath, (err) => {
//     if (err) {
//       console.error("Error :", err)
//       return
//     }

//     const fileNumbers = Array.from(
//       { length: numFiles },
//       (curr, idx) => {
//        return idx + 1
//       }
//     )

//     const createFile = (fileNumber) => {
//       const filename = `file${fileNumber}.json`
//       const filePath = path.join(directoryPath, filename)
//       const randomData = { value: Math.random() }

//       fs.writeFile(filePath, JSON.stringify(randomData), (err) => {
//         if (err) {
//           console.error(`Error creating ${filename}:`, err)
//           return
//         }
//         console.log(`Created ${filename}`)
//       })
//     }

//    fileNumbers.map(createFile)

//     fs.readdir(directoryPath, (err, files) => {
//       if (err) {
//         console.error("Error :", err)
//         return
//       }

//       const deleteFile = (filename) => {
//         const filePath = path.join(directoryPath, filename)
//         fs.unlink(filePath, (err) => {
//           if (err) {
//             console.error(`Error deleting ${filename}:`, err)
//             return
//           }
//           console.log(`Deleted ${filename}`)
//         })
//       }

//       files.map(deleteFile)
//     })
//   })
// }

// module.exports = problem1Fn


const fs = require("fs")
const path = require("path")

const problem1Fn = (directoryPath, numFiles) => {
  fs.mkdir(directoryPath, (err) => {
    if (err) {
      console.error("Error:", err)
      return
    }
    let countArr = []
    for (let idx = 1; idx <= numFiles; idx++) {
      const filename = `file${idx}.json`
      const filePath = path.join(directoryPath, filename)
      const randomData = { value: Math.random() }

      fs.writeFile(filePath, JSON.stringify(randomData), (err) => {
        if (err) {
          console.error(`Error creating ${filename}:`, err)
          return
        }
        console.log(`Created ${filename}`)
        countArr.push(filename)
        if (countArr.length === numFiles) {
          deleteFiles(directoryPath, numFiles)
        }
      })
    }
  })
}

const deleteFiles = (directoryPath, numFiles) => {
  for (let idx = 1; idx <= numFiles; idx++) {
    const filename = `file${idx}.json`
    const filePath = path.join(directoryPath, filename)

    fs.unlink(filePath, (err) => {
      if (err) {
        console.error(`Error deleting ${filename}:`, err)
        return
      }
      console.log(`Deleted ${filename}`)
    })
  }
}

module.exports = problem1Fn

