const fs = require('fs')
const path = require('path')


// Read the given file lipsum.txt
const tempFile =(lipsum)=>{

 fs.readFile(lipsum, 'utf8', (err, data) => {
  if (err) {
    console.error('Error:', err)
    return
  }

  // Convert the content to uppercase & write to a new file
  const uppercaseContent = data.toUpperCase()
  const newFileName = 'uppercase.txt'

  fs.writeFile(newFileName, uppercaseContent, (err) => {
    if (err) {
      console.error('Error :', err)
      return
    }

    //Read the new file, convert to lowercase, split into sentences, and write to a new file
    fs.readFile(newFileName, 'utf8', (err, newData) => {
      if (err) {
        console.error('Error:', err)
        return
      }

      const lowercaseContent = newData.toLowerCase()
      const sentences = lowercaseContent.split('. ')
      const sentenceFileName = 'sentences.txt'

      fs.writeFile(sentenceFileName, sentences.join('\n'), (err) => {
        if (err) {
          console.error('Error:', err)
          return
        }

        //Read the new files, sort the content, and write it out to a new file
        fs.readFile(sentenceFileName, 'utf8', (err, sortedData) => {
          if (err) {
            console.error('Error :', err)
            return
          }

          const sortedContent = sortedData.split('\n').sort().join('\n')
          const sortedFileName = 'sorted.txt'

          fs.writeFile(sortedFileName, sortedContent, (err) => {
            if (err) {
              console.error('Error:', err)
              return
            }

            //Write the filenames to filenames.txt and delete the files simultaneously
            const filenames = [newFileName, sentenceFileName, sortedFileName]
            const filenamesContent = filenames.join('\n')
            const filenamesFileName = 'filenames.txt'

            fs.writeFile(filenamesFileName, filenamesContent, (err) => {
              if (err) {
                console.error('Error:', err)
                return
              }

              console.log('Filenames written to filenames.txt')

              filenames.map((filename) => {
                return fs.unlink(filename, (err) => {
                  if (err) {
                    console.error(`Error deleting file '${filename}':`, err)
                  } else {
                    console.log(`File '${filename}' deleted successfully.`)
                  }
                })
              })
            })
          })
        })
      })
    })
  })
})
}

module.exports = tempFile